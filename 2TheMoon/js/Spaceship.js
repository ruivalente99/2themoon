/*
 * Criação dos objetos e as funções
 * Nave Espacial: Objeto Complexo
 * Composto por Piloto + Nave
 * Piloto:  Objeto Complexo (Pilot.js)
 * - Cabeça
 * * - Orelhas E & D
 * * - Oculos
 * - Corpo
 *
 * Nave:   Objeto Complexo
 * - Cabine
 * - Motor(frente)
 * - Cockpit/Vidro
 * - Asas E & D
 */

/* 
  Função para criar o objeto, 
  objeto único, logo nao precisamos de 
  chamar varias vezes a função

*/
var spaceship = function () {
  // this.mesh = spaceship.mesh = Grupo dos elementos
  this.mesh = new THREE.Object3D();
  this.mesh.name = "spaceship";

  // Cabin
  // Loader das texturas
  const loader = new THREE.TextureLoader();
  //Geometria
  const geomCabin = new THREE.CylinderGeometry(10, 35, 90, 10);
  //Material
  var matCabin = new THREE.MeshBasicMaterial({
    //Imagem da textura
    map: loader.load("./imgs/ship.png"),
    //Sombra
    shading: THREE.FlatShading,
  });
  var geomEsfera = new THREE.SphereGeometry( 5, 32, 32 );
  var esfera = new THREE.Mesh(geomEsfera, matCabin);
  esfera.position.set(35, 100, 100);
esfera.scale.set(20,20,20);
  esfera.mesh = new THREE.Mesh(geomEsfera,matCabin);  
  this.mesh.add(esfera);
  //Objeto da cabine
  var cabin = new THREE.Mesh(geomCabin, matCabin);
  cabin.castShadow = true;
  cabin.receiveShadow = true;
  cabin.rotation.z = Math.PI / 2;
  cabin.rotation.x = Math.PI / 4;
  this.mesh.add(cabin);

  // Motor/Frente
  const geomEngine = new THREE.CylinderGeometry(0, 35, 40, 15);
  var matEngine = new THREE.MeshBasicMaterial({
    color: Colors.yellow,
    shading: THREE.FlatShading,
  });
  var engine = new THREE.Mesh(geomEngine, matEngine);
  engine.rotation.z = -Math.PI / 2;
  engine.rotation.x = Math.PI / 4;
  engine.position.x = 65;
  engine.castShadow = true;
  engine.receiveShadow = true;

  this.mesh.add(engine);

  // Asas
  // Geometria
  var geomSideWing = new THREE.BoxGeometry(30, 5, 65, 1, 1, 1);
  //Material
  var matSideWing = new THREE.MeshBasicMaterial({
    color: Colors.red,
    shading: THREE.FlatShading,
  });
  //Objeto da asa Direita
  var sideWingD = new THREE.Mesh(geomSideWing, matSideWing);
  sideWingD.position.set(10, 5, 15);
  sideWingD.rotation.y = -Math.PI / 4;
  sideWingD.castShadow = true;
  sideWingD.receiveShadow = true;
  this.mesh.add(sideWingD);
  //Objeto da asa Esquerda
  var sideWingE = new THREE.Mesh(geomSideWing, matSideWing);
  sideWingE.position.set(10, 5, -15);
  sideWingE.rotation.y = Math.PI / 4;
  sideWingE.castShadow = true;
  sideWingE.receiveShadow = true;
  this.mesh.add(sideWingE);

  // Cockpit/Vidro
  //Geometria
  var geomWindshield = new THREE.CylinderGeometry(10, 15, 25, 15);
  // Material
  var matWindshield = new THREE.MeshBasicMaterial({
    color: Colors.white,
    transparent: true,
    opacity: 0.25,
    shading: THREE.FlatShading,
  });
  //Objeto do vidro
  var windshield = new THREE.Mesh(geomWindshield, matWindshield);
  windshield.position.set(1, 25, 0);
  windshield.castShadow = true;
  windshield.receiveShadow = true;
  this.mesh.add(windshield);

  //Piloto
  this.pilot = new Pilot();
  this.pilot.mesh.position.set(1, 30, 0);
  this.mesh.add(this.pilot.mesh);

  this.mesh.castShadow = true;
  this.mesh.receiveShadow = true;
};
