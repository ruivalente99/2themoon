//Cores mais usadas no jogo
var Colors = {
  red: 0xf25346,
  white: 0xd8d0d1,
  brown: 0x59332e,
  brownDark: 0x23190f,
  pink: 0xf5986e,
  yellow: 0xf4ce93,
  blue: 0x68c3c0,
};

/*
 * Variáveis dos objetos
 */
var playerSettings;
var asteroidsSettings;
var coinSettings;
/*
 * Variáveis do jogo
 */
var gameSettings;
var cameraSettings;

//Importante para os moviemntos
var deltaTime = 0;
var newTime = new Date().getTime();
var oldTime = new Date().getTime();

/*
 * Arrays dos objetos
 */
var asteroidsPool = [];
var particlesPool = [];
var particlesInUse = [];

/*
 * Resetar o jogo
 */
function resetGame() {
  activeCamera = false;
  shadowMode = false;
  playerSettings = {
    spaceshipDefaultHeight: 125,
    spaceshipAmpHeight: 80,
    spaceshipAmpWidth: 75,
    spaceshipMoveSensivity: 0.005,
    spaceshipRotXSensivity: 0.0008,
    spaceshipRotZSensivity: 0.0004,
    spaceshipFallSpeed: 0.001,
    spaceshipMinSpeed: 1,
    spaceshipMaxSpeed: 3.6,
    spaceshipSpeed: 0,
    spaceshipCollisionDisplacementX: 0,
    spaceshipCollisionSpeedX: 0,

    spaceshipCollisionDisplacementY: 0,
    spaceshipCollisionSpeedY: 0,
    spaceshipDefaultHeight: 125,
    spaceshipAmpHeight: 80,
    spaceshipAmpWidth: 75,
    spaceshipMoveSensivity: 0.005,
    spaceshipRotXSensivity: 0.0008,
    spaceshipRotZSensivity: 0.0004,
    spaceshipFallSpeed: 0.001,
    spaceshipMinSpeed: 1,
    spaceshipMaxSpeed: 3.6,
    spaceshipSpeed: 0,
    spaceshipCollisionDisplacementX: 0,
    spaceshipCollisionSpeedX: 0,

    spaceshipCollisionDisplacementY: 0,
    spaceshipCollisionSpeedY: 0,
  };
  asteroidsSettings = {
    asteroidDistanceTolerance: 20,
    asteroidValue: 10,
    asteroidsSpeed: 0.6,
    asteroidLastSpawn: 0,
    distanceForAsteroidsSpawn: 50,
  };
  coinSettings = {
    coinDistanceTolerance: 15,
    coinValue: 3,
    coinsSpeed: 0.5,
    coinLastSpawn: 0,
    distanceForCoinsSpawn: 100,
  };
  cameraSettings = {
    cameraFarPos: 1000,
    cameraNearPos: 150,
    cameraSensivity: 0.002,
  };
  moonSettings = {
    moonRadius: 1000,
    moonLength: 1000,
    moonRotationSpeed: 0.001,
  };
  gameSettings = {
    speed: 0,
    initSpeed: 0.00005,
    baseSpeed: 0.00005,
    targetBaseSpeed: 0.00015,
    incrementSpeedByTime: 0.0000005,
    incrementSpeedByLevel: 0.000005,
    distanceForSpeedUpdate: 100,
    speedLastUpdate: 0,

    distance: 0,
    ratioSpeedDistance: 50,
    energy: 100,
    ratioSpeedEnergy: 3,

    level: 1,
    levelLastUpdate: 0,
    distanceForLevelUpdate: 10000,

    status: "playing",
  };
}

/*
 * Ambiente
 */
//Camera
var scene,
  camera,
  fieldOfView,
  aspectRatio,
  nearPlane,
  farPlane,
  renderer,
  container;
//Musica
//Musica de funda
var music = new Audio("./sounds/music.mp3");
//Som da moeda
var soundCoin = new Audio("./sounds/coin.mp3");
/*
 * Criar os sons
 */
function createSounds() {
  soundCoin;
  soundCoin.volume = 0.01;
  music.volume = 0.05;
}

/*
 * Janela
 */

var HEIGHT,
  WIDTH,
  mousePos = { x: 0, y: 0 };

/*
 * Criar a cena
 */
function createScene() {
  HEIGHT = window.innerHeight;
  WIDTH = window.innerWidth;
  scene = new THREE.Scene();
  nearPlane = 0.1;
  farPlane = 10000;
  createOrthoCamera();
  scene.fog = new THREE.Fog(0xf7d9aa, 100, 950);
  renderer = new THREE.WebGLRenderer({ alpha: true, antialias: true });
  renderer.setSize(WIDTH, HEIGHT);
  renderer.shadowMap.enabled = true;
  container = document.getElementById("world");
  container.appendChild(renderer.domElement);
  window.addEventListener("resize", handleWindowResize, false);
}

function createOrthoCamera() {
  camera = new THREE.OrthographicCamera(
    WIDTH / -7,
    WIDTH / 7,
    HEIGHT / 7,
    HEIGHT / -7,
    nearPlane,
    farPlane
  );
  camera.position.x = 0;
  camera.position.z = 200;
  camera.position.y = playerSettings.spaceshipDefaultHeight;
  document.getElementById("cam").innerHTML = "🎥";
  activeCamera = false;
}
function createPrespCamera() {
  aspectRatio = WIDTH / HEIGHT;
  camera = new THREE.PerspectiveCamera(
    fieldOfView,
    aspectRatio,
    nearPlane,
    farPlane
  );
  camera.rotation.y = -Math.PI / 2;
  camera.position.x = -200;
  camera.position.y = playerSettings.spaceshipDefaultHeight * 2;
  document.getElementById("cam").innerHTML = "📷";
  activeCamera = true;
}
/*
 * Rezise da janela
 */
function handleWindowResize() {
  HEIGHT = window.innerHeight;
  WIDTH = window.innerWidth;
  renderer.setSize(WIDTH, HEIGHT);
  camera.aspect = WIDTH / HEIGHT;
  camera.updateProjectionMatrix();
  if (!activeCamera) createOrthoCamera();
}

/*
 * Eventos movimentos do rato
 */
function handleMouseMove(event) {
  var tx = -1 + (event.clientX / WIDTH) * 2;
  var ty = 1 - (event.clientY / HEIGHT) * 2;
  mousePos = { x: tx, y: ty };
}

function handleTouchMove(event) {
  event.preventDefault();
  var tx = -1 + (event.touches[0].pageX / WIDTH) * 2;
  var ty = 1 - (event.touches[0].pageY / HEIGHT) * 2;
  mousePos = { x: tx, y: ty };
}

function handleMouseUp(event) {
  if (gameSettings.status == "waitingReplay") {
    resetGame();
    hideReplay();
  }
}

function handleTouchEnd(event) {
  if (gameSettings.status == "waitingReplay") {
    resetGame();
    hideReplay();
  }
}

/*
 * "JOGAR" carregado
 */
function playPressed() {
  init();
  document.getElementById("plbt").style.display = "none";
  document.getElementById("htp").style.display = "none";
  document.getElementById("score").style.display = null;
}

var ambientLight, hemisphereLight, shadowLight, lanternLight;
/*
 * Criar as luzes
 */
function createLights() {
  // Dar uma luz sobre a cena
  hemisphereLight = new THREE.HemisphereLight(0xaaaaaa, 0x000000, 0.9);
  // Luz ambiente
  ambientLight = new THREE.AmbientLight(0xdc8874, 0.5);

  // Luz apontada para a cena
  /*
  *  / Luz 
    /   /
   /    \ 
  / Nave \
 /  Lua   \ 
  */
  shadowLight = new THREE.DirectionalLight(0xffffff, 0.9);
  shadowLight.position.set(150, 350, 350);
  shadowLight.castShadow = true;
  shadowLight.shadow.camera.left = -400;
  shadowLight.shadow.camera.right = 400;
  shadowLight.shadow.camera.top = 400;
  shadowLight.shadow.camera.bottom = -400;
  shadowLight.shadow.camera.near = 1;
  shadowLight.shadow.camera.far = 1000;
  shadowLight.shadow.mapSize.width = 4096;
  shadowLight.shadow.mapSize.height = 4096;
  //Lanterna, unica forma de ver asteroides no modo 🌙
  lanternLight = new THREE.SpotLight(Colors.yellow);
  lanternLight.intensity = 10;
  lanternLight.penumbra = 1;
  lanternLight.angle = -Math.PI / 2;

  //Add à cena
  scene.add(hemisphereLight);
  scene.add(shadowLight);
  scene.add(ambientLight);
  scene.add(lanternLight);
  scene.add(lanternLight.target);
}

var activeCamera;
var shadowMode;
//Mudar Cam e Luzes
document.addEventListener("keydown", (ev) => {
  if (ev.key == "c")
    if (camera != null && spaceship != null)
      activeCamera ? createOrthoCamera() : createPrespCamera();
  if (ev.key == "l") turnLights(shadowMode); // get all elements
});

/*
 * Funções auxiliares do evento
 */
//Mudar luzes
function turnLights(v) {
  shadowLight.visible = v;
  hemisphereLight.visible = v;
  ambientLight.visible = v;
  shadowMode = !v;
  v
    ? (document.getElementById("mode").innerHTML = "☀️")
    : (document.getElementById("mode").innerHTML = "🌙");
  v ? changeBackground(null) : changeBackground("#000000");
}
//Mudar fundo
function changeBackground(v) {
  var elements = document.getElementsByClassName("game-holder");
  for (var i = 0; i < elements.length; i++) {
    elements[i].style.background = v;
  }
}

// Objetos
var moon;
var spaceship;

/*
* Criar Nave / Spaceship.js
*/
function createSpaceShip() {
  spaceship = new spaceship();
  spaceship.mesh.scale.set(0.25, 0.25, 0.25);
  spaceship.mesh.position.y = playerSettings.spaceshipDefaultHeight;
  scene.add(spaceship.mesh);
}

/*
* Criar Lua
*/
function createMoon() {
  moon = new moon();
  moon.mesh.position.y = -moonSettings.moonRadius;
  scene.add(moon.mesh);
}

/*
* Criar Moedas / Coin.js
*/
function createCoins() {
  coinsHolder = new CoinsHolder(20);
  scene.add(coinsHolder.mesh);
}

/*
* Criar Asteróides / Asteroid.js
*/
function createAsteroids() {
  for (var i = 0; i < 10; i++) {
    var asteroid = new Asteroid();
    asteroidsPool.push(asteroid);
  }
  AsteroidsHolder = new AsteroidsHolder();
  scene.add(AsteroidsHolder.mesh);
}

/*
* Criar Particulas / Particule.js
*/
function createParticles() {
  for (var i = 0; i < 10; i++) {
    var particle = new Particle();
    particlesPool.push(particle);
  }
  particlesHolder = new ParticlesHolder();
  scene.add(particlesHolder.mesh);
}

function loop() {
  newTime = new Date().getTime();
  deltaTime = newTime - oldTime;
  oldTime = newTime;

  if (gameSettings.status == "playing") {
   
    //Spawn de moedas
    if (
      Math.floor(gameSettings.distance) % coinSettings.distanceForCoinsSpawn ==
        0 &&
      Math.floor(gameSettings.distance) > coinSettings.coinLastSpawn
    ) {
      coinSettings.coinLastSpawn = Math.floor(gameSettings.distance);
      coinsHolder.spawnCoins();
    }

     //Spawn de Asteroides
     if (
      Math.floor(gameSettings.distance) %
        asteroidsSettings.distanceForAsteroidsSpawn ==
        0 &&
      Math.floor(gameSettings.distance) > asteroidsSettings.asteroidLastSpawn
    ) {
      asteroidsSettings.asteroidLastSpawn = Math.floor(gameSettings.distance);
      AsteroidsHolder.spawnAsteroides();
    }

    // Andar mais rapido
    if (
      Math.floor(gameSettings.distance) % gameSettings.distanceForSpeedUpdate ==
        0 &&
      Math.floor(gameSettings.distance) > gameSettings.speedLastUpdate
    ) {
      gameSettings.speedLastUpdate = Math.floor(gameSettings.distance);
      gameSettings.targetBaseSpeed +=
        gameSettings.incrementSpeedByTime * deltaTime;
    }

   
    if (
      Math.floor(gameSettings.distance) % gameSettings.distanceForLevelUpdate ==
        0 &&
      Math.floor(gameSettings.distance) > gameSettings.levelLastUpdate
    ) {
      gameSettings.levelLastUpdate = Math.floor(gameSettings.distance);
      gameSettings.level++;
      gameSettings.targetBaseSpeed =
        gameSettings.initSpeed +
        gameSettings.incrementSpeedByLevel * gameSettings.level;
    }

    updateSpaceship();
    updateDistance();
    updateEnergy();

    gameSettings.baseSpeed +=
      (gameSettings.targetBaseSpeed - gameSettings.baseSpeed) *
      deltaTime *
      0.02;
    gameSettings.speed = gameSettings.baseSpeed * playerSettings.spaceshipSpeed;
  } else if (gameSettings.status == "gameover") {
    gameSettings.speed *= 0.99;
    spaceship.mesh.rotation.z +=
      (-Math.PI / 2 - spaceship.mesh.rotation.z) * 0.0002 * deltaTime;
    spaceship.mesh.rotation.x += 0.0003 * deltaTime;
    playerSettings.spaceshipFallSpeed *= 1.05;
    spaceship.mesh.position.y -= playerSettings.spaceshipFallSpeed * deltaTime;

    if (spaceship.mesh.position.y < -200) {
      showReplay();
      gameSettings.status = "waitingReplay";
    }
  } else if (gameSettings.status == "waitingReplay") {
  }

  moon.mesh.rotation.z += gameSettings.speed * deltaTime; //*gameSettings.moonRotationSpeed;

  if (moon.mesh.rotation.z > 2 * Math.PI) moon.mesh.rotation.z -= 2 * Math.PI;

  ambientLight.intensity += (0.5 - ambientLight.intensity) * deltaTime * 0.005;

  coinsHolder.rotateCoins();
  AsteroidsHolder.rotateAsteroids();

  lanternLight.target.position.y = spaceship.mesh.clone().position.y + 30;
  lanternLight.target.position.x = spaceship.mesh.clone().position.x + 65;
  lanternLight.target.position.z = 0;
  renderer.render(scene, camera);
  requestAnimationFrame(loop);
}

function updateDistance() {
  gameSettings.distance +=
    gameSettings.speed * deltaTime * gameSettings.ratioSpeedDistance;
  fieldDistance.innerHTML = Math.floor(gameSettings.distance);
}

function updateSpaceship() {
  playerSettings.spaceshipSpeed = normalize(
    mousePos.x,
    -0.5,
    0.5,
    playerSettings.spaceshipMinSpeed,
    playerSettings.spaceshipMaxSpeed
  );
  var targetY = normalize(
    mousePos.y,
    -0.75,
    0.75,
    playerSettings.spaceshipDefaultHeight - playerSettings.spaceshipAmpHeight,
    playerSettings.spaceshipDefaultHeight + playerSettings.spaceshipAmpHeight
  );
  var targetX = normalize(
    mousePos.x,
    -1,
    1,
    -playerSettings.spaceshipAmpWidth * 0.7,
    -playerSettings.spaceshipAmpWidth
  );

  playerSettings.spaceshipCollisionDisplacementX +=
    playerSettings.spaceshipCollisionSpeedX;
  targetX += playerSettings.spaceshipCollisionDisplacementX;

  playerSettings.spaceshipCollisionDisplacementY +=
    playerSettings.spaceshipCollisionSpeedY;
  targetY += playerSettings.spaceshipCollisionDisplacementY;

  spaceship.mesh.position.y +=
    (targetY - spaceship.mesh.position.y) *
    deltaTime *
    playerSettings.spaceshipMoveSensivity;
  spaceship.mesh.position.x +=
    (targetX - spaceship.mesh.position.x) *
    deltaTime *
    playerSettings.spaceshipMoveSensivity;

  spaceship.mesh.rotation.z =
    (targetY - spaceship.mesh.position.y) *
    deltaTime *
    playerSettings.spaceshipRotXSensivity;
  spaceship.mesh.rotation.x =
    (spaceship.mesh.position.y - targetY) *
    deltaTime *
    playerSettings.spaceshipRotZSensivity;
  var targetCameraZ = normalize(
    playerSettings.spaceshipSpeed,
    playerSettings.spaceshipMinSpeed,
    playerSettings.spaceshipMaxSpeed,
    cameraSettings.cameraNearPos,
    cameraSettings.cameraFarPos
  );
  camera.fov = normalize(mousePos.x, -1, 1, 40, 80);
  camera.updateProjectionMatrix();
  camera.position.y +=
    (spaceship.mesh.position.y - camera.position.y) *
    deltaTime *
    cameraSettings.cameraSensivity;

  playerSettings.spaceshipCollisionSpeedX +=
    (0 - playerSettings.spaceshipCollisionSpeedX) * deltaTime * 0.03;
  playerSettings.spaceshipCollisionDisplacementX +=
    (0 - playerSettings.spaceshipCollisionDisplacementX) * deltaTime * 0.01;
  playerSettings.spaceshipCollisionSpeedY +=
    (0 - playerSettings.spaceshipCollisionSpeedY) * deltaTime * 0.03;
  playerSettings.spaceshipCollisionDisplacementY +=
    (0 - playerSettings.spaceshipCollisionDisplacementY) * deltaTime * 0.01;

  // spaceship.pilot.updateHairs();
}

function showReplay() {
  replayMessage.style.display = "block";
}

function hideReplay() {
  replayMessage.style.display = "none";
}

function normalize(v, vmin, vmax, tmin, tmax) {
  var nv = Math.max(Math.min(v, vmax), vmin);
  var dv = vmax - vmin;
  var pc = (nv - vmin) / dv;
  var dt = tmax - tmin;
  var tv = tmin + pc * dt;
  return tv;
}

var fieldDistance, energyBar, replayMessage, fieldLevel, levelCircle;
/*
 *Inicio do jogo
 */
function init(event) {
  //Distancia
  fieldDistance = document.getElementById("distValue");
  // Engeria d
  energyBar = document.getElementById("energyBar");
  // Mensagem de replay
  replayMessage = document.getElementById("replayMessage");

  // Reseta jogo
  resetGame();

  createScene();
  createSpaceShip();
  music.play();
  createLights();
  createSounds();
  createMoon();
  createCoins();
  createAsteroids();
  createParticles();
  document.addEventListener("mousemove", handleMouseMove, false);
  document.addEventListener("touchmove", handleTouchMove, false);
  document.addEventListener("mouseup", handleMouseUp, false);
  document.addEventListener("touchend", handleTouchEnd, false);
  loop();
}
