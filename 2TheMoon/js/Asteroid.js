/*
 * Criação dos objetos e as funções
 *  Asteroide: Objeto simples
 *  Não existe hierarquia 
 */

/* Função para criar o objeto */
Asteroid = function () {
   // Loader das texturas
  const loader = new THREE.TextureLoader();
  // Geometria
  var geom = new THREE.TetrahedronGeometry(13, 2);
  // Material 
  var mat = new THREE.MeshPhongMaterial({
    //Imagem da textura
    map: loader.load("./imgs/rock.png"),
    // Cor
    color: Colors.brownDark,
    // Brilho
    shininess: 0.2,
    specular: Colors.yellow,
    // Sombra
    shading: THREE.FlatShading,
  });
  // this.mesh = Asteroide.mesh = Objeto
  this.mesh = new THREE.Mesh(geom, mat);
  this.mesh.castShadow = true;
  this.angle = 0;
  this.dist = 0;
};

/* Função para "armazenar" os asteroides em grupo */
AsteroidsHolder = function () {
  // this.mesh, "grupo" para guardar os asteroides
  this.mesh = new THREE.Object3D();
  // asteroides em uso
  this.asteroidsInUse = [];
};

/* Função para "spawnar" os asteroides  */
AsteroidsHolder.prototype.spawnAsteroides = function () {
  // Número de asteroides depende do nível, o nivel aumenta com a distância
  var nAsteroids = gameSettings.level * 1.5;

  for (var i = 0; i < nAsteroids; i++) {
    var asteroid;
    if (asteroidsPool.length) {
      asteroid = asteroidsPool.pop(); // .pop remove o ultimo elemento
    } else {
      asteroid = new Asteroid(); // criar novo asteroide
    }
    // Posição, anglos e distância
    asteroid.angle = -(i * 0.1);
    asteroid.distance =
      moonSettings.moonRadius +
      playerSettings.spaceshipDefaultHeight +
      (-1 + Math.random() * 2) * (playerSettings.spaceshipAmpHeight - 20);
    asteroid.mesh.position.y =
      -moonSettings.moonRadius + Math.sin(asteroid.angle) * asteroid.distance;
    asteroid.mesh.position.x = Math.cos(asteroid.angle) * asteroid.distance;
    // Conjunto de asteroides spawnados
    this.mesh.add(asteroid.mesh);
    // .push colocar o asteroide no array
    this.asteroidsInUse.push(asteroid);
  }

};

/* Função para "rodar" os asteroides  */
AsteroidsHolder.prototype.rotateAsteroids = function () {
  for (var i = 0; i < this.asteroidsInUse.length; i++) {
    //Rotação
    var asteroid = this.asteroidsInUse[i];
    asteroid.angle +=
      gameSettings.speed * deltaTime * asteroidsSettings.asteroidsSpeed;

    if (asteroid.angle > Math.PI * 2) asteroid.angle -= Math.PI * 2;

    asteroid.mesh.position.y =
      -moonSettings.moonRadius + Math.sin(asteroid.angle) * asteroid.distance;
    asteroid.mesh.position.x = Math.cos(asteroid.angle) * asteroid.distance;
    asteroid.mesh.rotation.z += Math.random() * 0.1;
    asteroid.mesh.rotation.y += Math.random() * 0.1;

    /*
      Calculo para saber se o asteroide colidiu
      Se a subtração de distância do asteroide e da nave for inferior à tolerância, colidiu
    */
    var diffPos = spaceship.mesh.position
      .clone() // Recomendação de um forum https://discourse.threejs.org/t/how-to-perform-a-deep-clone-of-a-scene/5408/5
      .sub(asteroid.mesh.position.clone()); // Subtração do Vector3 https://threejs.org/docs/#api/en/math/Vector3
    var d = diffPos.length();
    if (d < asteroidsSettings.asteroidDistanceTolerance) {
      // Spawnar particles pós colisão
      particlesHolder.spawnParticles(
        asteroid.mesh.position.clone(),
        25,
        Colors.brownDark,
        3
      );
      
      asteroidsPool.unshift(this.asteroidsInUse.splice(i, 1)[0]); // Mover asteroides intactos para o inicio do array
      this.mesh.remove(asteroid.mesh); // Remover o asteroide atingido
      playerSettings.spaceshipCollisionSpeedX = (100 * diffPos.x) / d;
      playerSettings.spaceshipCollisionSpeedY = (100 * diffPos.y) / d;
      ambientLight.intensity = 2;
      //Remover energia, gas
      removeEnergy();
      i--;
    } else if (asteroid.angle > Math.PI) {
      // Se o asteroid sair de cena, é eliminado
      asteroidsPool.unshift(this.asteroidsInUse.splice(i, 1)[0]);
      this.mesh.remove(asteroid.mesh);
      i--;
    }
  }
};
