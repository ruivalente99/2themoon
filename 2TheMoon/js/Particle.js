/*
 * Criação dos objetos e as funções
 *  Particula: Objeto simples
 *  Depende apenas da colisão da nave com outro objeto
 */


/* Função para criar o objeto */
Particle = function () {
  // Geometria
  var geom = new THREE.TetrahedronGeometry(3, 0);
   // Material 
  var mat = new THREE.MeshPhongMaterial({
    // Cor
    color: 0xffffff,
    // Brilho
    shininess: 0,
    specular: 0xffffff,
    // Sombra
    shading: THREE.FlatShading,
  });
  // this.mesh = Particle.mesh = Objeto
  this.mesh = new THREE.Mesh(geom, mat);
};

/* Função para "armazenar" as particulas em grupo */
ParticlesHolder = function () {
  // this.mesh, "grupo" para guardar as particulas
  this.mesh = new THREE.Object3D();
  // particulas em uso
  this.particlesInUse = [];
};

/* Função para explodir as particulas */
Particle.prototype.explode = function (pos, color, scale) {
  var _this = this;
  var _p = this.mesh.parent;
  // Material
  this.mesh.material.color = new THREE.Color(color);
  this.mesh.material.needsUpdate = true;
  // Escala
  this.mesh.scale.set(scale, scale, scale);
  // Posição
  var targetX = pos.x + (-1 + Math.random() * 2) * 50;
  var targetY = pos.y + (-1 + Math.random() * 2) * 50;
  var speed = 0.6 + Math.random() * 0.2;
  // Animação
  TweenMax.to(this.mesh.rotation, speed, {
    x: Math.random() * 12,
    y: Math.random() * 12,
  });
  TweenMax.to(this.mesh.scale, speed, { x: 0.1, y: 0.1, z: 0.1 });
  TweenMax.to(this.mesh.position, speed, {
    x: targetX,
    y: targetY,
    delay: Math.random() * 0.1,
    ease: Power2.easeOut,
    onComplete: function () {
      if (_p) _p.remove(_this.mesh);
      _this.mesh.scale.set(1, 1, 1);
      particlesPool.unshift(_this);
    },
  });
};

/* Função para "spawnar" as particulas  */
ParticlesHolder.prototype.spawnParticles = function (
  pos,
  density,
  color,
  scale
) {
  // Número de particulas depende da densidade
  var nPArticles = density;

  for (var i = 0; i < nPArticles; i++) {
    var particle;
    if (particlesPool.length) {
      particle = particlesPool.pop(); // .pop remove o ultimo elemento
    } else {
      particle = new Particle(); // criar nova particula
    }
    // Conjunto de particulas spawnadas
    this.mesh.add(particle.mesh);
    particle.mesh.visible = true;
    var _this = this;
    // Posição
    particle.mesh.position.y = pos.y;
    particle.mesh.position.x = pos.x;
    // Explodir
    particle.explode(pos, color, scale);
  }
};
