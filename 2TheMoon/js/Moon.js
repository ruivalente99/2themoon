/*
 * Criação dos objetos e as funções
 *  Lua: Objeto simples
 *  Não existe hierarquia 
 */

/* Função para criar o objeto */
moon = function () {
  // Loader das texturas
  const loader = new THREE.TextureLoader();
  var tt = loader.load("./imgs/moon.png");
  tt.wrapS = THREE.RepeatWrapping;
  tt.wrapT = THREE.RepeatWrapping;
  tt.repeat.set(3, 3);
  // Geometrica
  var geom = new THREE.CylinderGeometry(
    moonSettings.moonRadius,
    moonSettings.moonRadius,
    moonSettings.moonLength,
    250
  );
  geom.applyMatrix(new THREE.Matrix4().makeRotationX(-Math.PI / 2));
  geom.mergeVertices();
  var l = geom.vertices.length;

 // Material 
  var mat = new THREE.MeshPhongMaterial({
    // Cor
    color: "#606060",
    //Imagem da textura
    map: tt,
    // Opacidade da cor
    opacity: 0.1,
  });
  // this.mesh = moon.mesh = Objeto
  this.mesh = new THREE.Mesh(geom, mat);
  this.mesh.name = "moon";
  this.mesh.receiveShadow = true;
};
