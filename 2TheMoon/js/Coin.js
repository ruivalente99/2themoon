/*
 * Criação dos objetos e as funções
 *  Moeda: Objeto simples
 *  Não existe hierarquia 
 */

/* Função para criar o objeto */
Coin = function () {
   // Loader das texturas
  const loader = new THREE.TextureLoader();
  const textureCoin = loader.load("./imgs/dogecoin.png");
  // Geometrica
  var geom = new THREE.CylinderGeometry(6,6,0.4,100);
  // Material 
  var mat = new THREE.MeshPhongMaterial({
    //Imagem da textura
    map: textureCoin,
  });
  // this.mesh = Coin.mesh = Objeto
  this.mesh = new THREE.Mesh(geom, mat);
  //this.mesh = createCoin();
  this.mesh.castShadow = true;
  this.angle = 0;
  this.dist = 0;
};
function createCoin()
{
  var coin_sides_geo =
  new THREE.CylinderGeometry(6,6,1,50);
var coin_cap_geo = new THREE.Geometry();
var r = 6.0;
for (var i=0; i<100; i++) {
  var a = i * 1/100 * Math.PI * 2;
  var z = Math.sin(a);
  var x = Math.cos(a);
  var a1 = (i+1) * 1/100 * Math.PI * 2;
  var z1 = Math.sin(a1);
  var x1 = Math.cos(a1);
  coin_cap_geo.vertices.push(
    new THREE.Vertex(new THREE.Vector3(0, 0, 0)),
    new THREE.Vertex(new THREE.Vector3(x*r, 0, z*r)),
    new THREE.Vertex(new THREE.Vector3(x1*r, 0, z1*r))
  );
  coin_cap_geo.faceVertexUvs[0].push([
    new THREE.UVMapping(0.5, 0.5),
    new THREE.UVMapping(x/2+0.5, z/2+0.5),
    new THREE.UVMapping(x1/2+0.5, z1/2+0.5)
  ]);
  coin_cap_geo.faces.push(new THREE.Face3(i*3, i*3+1, i*3+2));
}
coin_cap_geo.computeCentroids();
coin_cap_geo.computeFaceNormals();

var coin_sides_texture =
  THREE.ImageUtils.loadTexture("./imgs/dogecoin.png");
var coin_cap_texture =
  THREE.ImageUtils.loadTexture("./imgs/dogecoin.png");

var coin_sides_mat =
  new THREE.MeshLambertMaterial({map:coin_sides_texture});
var coin_sides =
  new THREE.Mesh( coin_sides_geo, coin_sides_mat );

var coin_cap_mat = new THREE.MeshLambertMaterial({map:coin_cap_texture});
var coin_cap_top = new THREE.Mesh( coin_cap_geo, coin_cap_mat );
var coin_cap_bottom = new THREE.Mesh( coin_cap_geo, coin_cap_mat );
coin_cap_top.position.y = 0.5;
coin_cap_bottom.position.y = -0.5;
coin_cap_top.rotation.x = Math.PI;

var coin = new THREE.Object3D();
coin.add(coin_sides);
coin.add(coin_cap_top);
coin.add(coin_cap_bottom);
return coin;
}
/* Função para "armazenar" as moedas em grupo */
CoinsHolder = function (nCoins) {
  // this.mesh, "grupo" para guardar as moedas
  this.mesh = new THREE.Object3D();
  // moedas em uso
  this.coinsInUse = [];
  // array das moedas em fila
  this.coinsPool = [];
  for (var i = 0; i < nCoins; i++) {
    var coin = new Coin();
    this.coinsPool.push(coin);
  }
};

/* Função para "spawnar" as moedas  */
CoinsHolder.prototype.spawnCoins = function () {
  // Número de moedas random, não varia com o nivel
  var nCoins = 1 + Math.floor(Math.random() * 10);
  var d =
    moonSettings.moonRadius +
    playerSettings.spaceshipDefaultHeight +
    (-1 + Math.random() * 2) * (playerSettings.spaceshipAmpHeight - 20);
  var amplitude = 10 + Math.round(Math.random() * 10);
  for (var i = 0; i < nCoins; i++) {
    var coin;
    if (this.coinsPool.length) {
      coin = this.coinsPool.pop(); // .pop remove o ultimo elemento
    } else {
      coin = new Coin();  // criar uma nova moeda
    }
    // Posição, anglos e distância
    coin.angle = -(i * 0.02);
    coin.distance = d + Math.cos(i * 0.5) * amplitude;
    coin.mesh.position.y =
      -moonSettings.moonRadius + Math.sin(coin.angle) * coin.distance;
    coin.mesh.position.x = Math.cos(coin.angle) * coin.distance;
    // Conjunto de moedas spawnados
    this.mesh.add(coin.mesh);
    // .push colocar a meoda no array
    this.coinsInUse.push(coin);
  }
};

/* Função para "rodar" os moedas  */
CoinsHolder.prototype.rotateCoins = function () {
  for (var i = 0; i < this.coinsInUse.length; i++) {
    //Rotação
    var coin = this.coinsInUse[i];
    if (coin.exploding) continue;
    coin.angle += gameSettings.speed * deltaTime * coinSettings.coinsSpeed;
    if (coin.angle > Math.PI * 2) coin.angle -= Math.PI * 2;
    coin.mesh.position.y =
      -moonSettings.moonRadius + Math.sin(coin.angle) * coin.distance;
    coin.mesh.position.x = Math.cos(coin.angle) * coin.distance;
    coin.mesh.rotation.z += Math.random() * 0.1;
    coin.mesh.rotation.y += Math.random() * 0.1;

    /*
      Calculo para saber se a moeda colidiu
      Se a subtração de distância do moeda e da nave for inferior à tolerância, colidiu
    */
    var diffPos = spaceship.mesh.position
      .clone() // Recomendação de um forum https://discourse.threejs.org/t/how-to-perform-a-deep-clone-of-a-scene/5408/5
      .sub(coin.mesh.position.clone()); // Subtração do Vector3 https://threejs.org/docs/#api/en/math/Vector3
    var d = diffPos.length();
    if (d < coinSettings.coinDistanceTolerance) {
      // Spawnar particles pós colisão
      soundCoin.play();
      particlesHolder.spawnParticles(
        coin.mesh.position.clone(),
        5,
        Colors.yellow,
        0.8
      );

      this.coinsPool.unshift(this.coinsInUse.splice(i, 1)[0]); // Mover moedas intactas para o inicio do array
      this.mesh.remove(coin.mesh); // Remover a moeda atingida
      //Adiciona energia, gas
      addEnergy();
      i--;
    } else if (coin.angle > Math.PI) {
       // Se a moeda sair de cena, é eliminado
      this.coinsPool.unshift(this.coinsInUse.splice(i, 1)[0]);
      this.mesh.remove(coin.mesh);
      i--;
    }
  }
};
