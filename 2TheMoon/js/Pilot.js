/*
 * Criação dos objetos e as funções
 * Piloto: Objeto Complexo
 * Piloto:
 * - Cabeça
 * * - Orelhas E & D
 * * - Oculos
 * - Corpo
 */

/* Função para criar o objeto */
var Pilot = function () {
  // this.mesh = Pilot.mesh = Grupo dos elementos
  this.mesh = new THREE.Object3D();
  this.mesh.name = "pilot";

  // Corpo
  // Geometria
  var bodyGeom = new THREE.BoxGeometry(15, 15, 15);
  // Material
  var bodyMat = new THREE.MeshPhongMaterial({
    color: Colors.white,
    shading: THREE.FlatShading,
  });
  // Objeto do corpo
  var body = new THREE.Mesh(bodyGeom, bodyMat);
  body.position.set(2, -12, 0);
  this.mesh.add(body);

  // Cabeça
  // Geometria
  var headGeom = new THREE.BoxGeometry(10, 10, 10);
  // Material
  var headMat = new THREE.MeshLambertMaterial({ color: Colors.white });
  // Objeto do corpo
  var head = new THREE.Mesh(headGeom, headMat);
  this.mesh.add(head);

  //Oculos

  // Geometria
  var glassGeom = new THREE.BoxGeometry(5, 5, 5);
  // Material
  var glassMat = new THREE.MeshLambertMaterial({ color: 0x000000 });

  var glassR = new THREE.Mesh(glassGeom, glassMat);
  glassR.position.set(6, 0, 3);

  var glassL = glassR.clone();
  glassL.position.z = -glassR.position.z;

  // Geometria
  var glassAGeom = new THREE.BoxGeometry(11, 1, 11);
  var glassA = new THREE.Mesh(glassAGeom, glassMat);
  this.mesh.add(glassR);
  this.mesh.add(glassL);
  this.mesh.add(glassA);

  //Orelhas
  // Geometria
  var earGeom = new THREE.BoxGeometry(2, 3, 2);
  var earL = new THREE.Mesh(earGeom, headMat);
  earL.position.set(0, 0, -6);
  var earR = earL.clone();
  earR.position.set(0, 0, 6);
  this.mesh.add(earL);
  this.mesh.add(earR);
};
