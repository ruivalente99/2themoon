var blinkEnergy = false;
/*
* Atualzar energia
*/
function updateEnergy() {
  gameSettings.energy -=
    gameSettings.speed * deltaTime * gameSettings.ratioSpeedEnergy;
  gameSettings.energy = Math.max(0, gameSettings.energy);
  energyBar.style.right = 100 - gameSettings.energy + "%";
  energyBar.style.backgroundColor =
    gameSettings.energy < 50 ? "#f25346" : "#68c3c0";

  if (gameSettings.energy < 30) {
    energyBar.style.animationName = "blinking";
  } else {
    energyBar.style.animationName = "none";
  }

  if (gameSettings.energy < 1) {
    gameSettings.status = "gameover";
  }
}

/*
* Adicionar energia
*/
function addEnergy() {
  playerSettings.energy += coinSettings.coinValue;
  playerSettings.energy = Math.min(playerSettings.energy, 100);
}

/*
* Remover energia
*/
function removeEnergy() {
  playerSettings.energy -= asteroidsSettings.asteroidValue;
  playerSettings.energy = Math.max(0, playerSettings.energy);
}
